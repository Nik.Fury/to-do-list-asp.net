﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToDoList.DataModel
{
    /// <summary>
    /// Класс содержащий все необходимые свойства для объекта задачи
    /// </summary>
    public class Task
    {
        /// <summary>
        /// Идентификатор задачи
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Название задачи
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Важность задачи
        /// </summary>
        public bool IsImportant { get; set; }

        /// <summary>
        /// Выполнено ли задание
        /// </summary>
        public bool IsDone { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Дата выполнения
        /// </summary>
        public DateTime? DoneDate { get; set; }
    }
}