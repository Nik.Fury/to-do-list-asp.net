﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToDoList.DataModel;
using ToDoList.ViewModels;

namespace ToDoList.Controllers
{
    public class TaskController : Controller
    {
        private TasksContext context = new TasksContext();

        // переменные хранят в себе текущие состояние фильтров
        private static bool isOnlyActive;
        private static bool isOnlyDone;
        private static string searchText;

        public ActionResult Index(string searchString, bool? onlyActive, bool? onlyDone)
        {
            var tasks = context.Tasks.ToList();

            var tasksVM = new TaskViewModel();

            // Выполнем поиск 
            if (!string.IsNullOrEmpty(searchString))
            {
                tasks = tasks.Where(x => x.Name.Contains(searchString)).ToList();
            }

            // Если отмечено показывать только активные - выбираем только их
            if (onlyActive.HasValue && onlyActive.Value)
            {
                tasks = tasks.Where(x => !x.IsDone).ToList();
            }

            // Если отмечено показывать только выполненные - выбираем только их
            if (onlyDone.HasValue && onlyDone.Value)
            {
                tasks = tasks.Where(x => x.IsDone).ToList();
            }

            tasksVM.Tasks = tasks;
            tasksVM.OnlyActive = onlyActive.HasValue && onlyActive.Value;
            tasksVM.OnlyDone = onlyDone.HasValue && onlyDone.Value;
            tasksVM.ActiveTasksCount = context.Tasks.Count(x => !x.IsDone);
            tasksVM.DoneTasksCount = context.Tasks.Count(x => x.IsDone);
            tasksVM.SearchText = searchString;

            // сохраняем значения фильтров
            isOnlyActive = tasksVM.OnlyActive;
            isOnlyDone = tasksVM.OnlyDone;
            searchText = tasksVM.SearchText;

            return View(tasksVM);
        }

        [HttpPost]
        public ActionResult New(string taskName)
        {
            if (string.IsNullOrEmpty(taskName))
                return RedirectToAction("Index");

            var task = new Task()
            {
                Name = taskName,
                IsDone = false,
                IsImportant = false,
                CreationDate = DateTime.Now
            };

            context.Tasks.Add(task);

            context.SaveChanges();


            return RedirectToAction("Index", new { searchString = searchText, onlyActive = isOnlyActive, onlyDone = isOnlyDone });
        }

        [Route("Delete/{id}")]
        public ActionResult Delete(int id)
        {
            var task = context.Tasks.FirstOrDefault(x => x.Id == id);
            if (task != null)
            {
                context.Tasks.Remove(task);
                context.SaveChanges();
            }

            return RedirectToAction("Index", new { searchString = searchText, onlyActive = isOnlyActive, onlyDone = isOnlyDone });
        }

        [Route("Important/{id}")]
        public ActionResult MarkAsImportant(int id)
        {
            var task = context.Tasks.FirstOrDefault(x => x.Id == id);
            if (task != null)
            {
                task.IsImportant = !task.IsImportant;
                context.SaveChanges();
            }

            return RedirectToAction("Index", new { searchString = searchText, onlyActive = isOnlyActive, onlyDone = isOnlyDone });
        }

        [Route("Done/{id}")]
        public ActionResult MarkAsDone(int id)
        {
            var task = context.Tasks.FirstOrDefault(x => x.Id == id);
            if (task != null)
            {
                task.IsDone = true;
                context.SaveChanges();
            }

            return RedirectToAction("Index", new { searchString = searchText, onlyActive = isOnlyActive, onlyDone = isOnlyDone });
        }


        protected override void Dispose(bool disposing)
        {
            context?.Dispose();
            base.Dispose(disposing);
        }
    }
}