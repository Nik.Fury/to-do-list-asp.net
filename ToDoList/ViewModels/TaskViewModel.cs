﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ToDoList.DataModel;

namespace ToDoList.ViewModels
{
    /// <summary>
    /// Модель представления для страницы с задачами
    /// </summary>
    public class TaskViewModel
    {
        /// <summary>
        /// Список отображаемых задач
        /// </summary>
        public IEnumerable<Task> Tasks { get; set; }

        /// <summary>
        /// Показывать только выполненные
        /// </summary>
        public bool OnlyDone { get; set; }

        /// <summary>
        /// Показывать только активные
        /// </summary>
        public bool OnlyActive { get; set; }

        /// <summary>
        /// Количество активных задач
        /// </summary>
        public int ActiveTasksCount { get; set; }

        /// <summary>
        /// Количество выполненных задач
        /// </summary>
        public int DoneTasksCount { get; set; }

        /// <summary>
        /// Текст поиска
        /// </summary>
        public string SearchText { get; set; }
    }
}